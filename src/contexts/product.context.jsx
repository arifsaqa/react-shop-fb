import { createContext, useContext, useEffect, useState } from "react";
import PRODUCTS from '../shop-data.json';


//actiual value you want to access
export const ProductContext = createContext({
  products:[]
});

export const useProductContext = () => useContext(ProductContext);

export const ProductProvider = ({ children }) => {
  const [products, setProducts] = useState(PRODUCTS);
  const value = { products, setProducts };

  return <ProductContext.Provider value={value}>{children}</ProductContext.Provider>
}