import { createContext, useContext, useEffect, useState } from "react";


//actiual value you want to access
export const CartContext = createContext({
  isCartOpen: false,
  setCartOpen: () => { },
  cartItems: [],
  addItems: () => { }
});

export const useCartContext = () => useContext(CartContext);

const addCartItem = (cartItems, productToAdd) => {

  const checkItem = cartItems.find((a) => a.id === productToAdd.id);

  if (checkItem) {
    console.log(cartItems);
    return cartItems.map((cartItem) => cartItem.id === productToAdd.id ? { ...cartItem, qty: cartItem.qty + 1 } : cartItem)
  }
  return [...cartItems, { ...productToAdd, qty: 1 }];
}

export const CartProvider = ({ children }) => {

  const [isCartOpen, setCartOpen] = useState(false);
  const [cartItems, setCartItems] = useState([{id:0, qty: 0 }]);
  const [countCartItems, setCountCartItems] = useState(0);

  const addItems = (productToAdd) => {
    setCartItems(addCartItem(cartItems, productToAdd),0);
  }
  useEffect(() => {
    let c = cartItems.reduce((prev, curr) => prev + curr.qty, 0);
    setCountCartItems(c);
  }, [cartItems]);
  const value = { isCartOpen, setCartOpen, cartItems, addItems, countCartItems };

  return <CartContext.Provider value={value}>{children}</CartContext.Provider>
}