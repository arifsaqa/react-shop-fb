import React from 'react'
import ProductCard from '../../components/product-card/product.component';
import { useProductContext } from '../../contexts/product.context';
import './shop.styles.scss';

function Shop() {
  const { products } = useProductContext();
  return (
    <div className='products-container'>{products.map((product) => (
      <ProductCard key={product.id} product={product} />
    ))}</div>
  )
}
export default Shop