import React from 'react'
import Directory from '../../components/directory/directory'
import categories from '../../dummies/categories.json'

function Home() {
  return (
    <>
      <Directory categories={categories}/>
    </>
  )
}

export default Home