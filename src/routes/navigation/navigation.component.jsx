import React, { Fragment } from 'react'
import { Link, Outlet } from 'react-router-dom'
import './navigation.styles.scss'
import { ReactComponent as CrownLogo } from '../../assets/crown.svg'
import { useUserContext } from '../../contexts/user.context'
import { signOutUser } from '../../utils/firebase/firebase.utils'
import CartIcon from '../../components/cart-icon/cart-icon.component'
import CartDropdown from '../../components/cart-dropdown/cart-dropdown.component'
import { useCartContext } from '../../contexts/cart.context'

function Navigation() {
  const { currentUser } = useUserContext();
  const { isCartOpen } = useCartContext();
  const signOutHandler = async () => {
    await signOutUser();
  }
  return (
    <Fragment>
      <div className="navigation">
        <Link to='/'>
          <CrownLogo className='logo' />
        </Link>
        <div className='nav-links-container'>
          <Link className='nav-link' to='/shop'>
            SHOP
          </Link>
          {currentUser ?
            <span className='nav-link' onClick={signOutHandler}>
              SIGN OUT
            </span> :
            <Link className='nav-link' to='/auth'>
              SIGN IN
            </Link>
          }
          <CartIcon />
        </div>
        {isCartOpen && <CartDropdown />}
      </div>
      <Outlet />
    </Fragment>
  )
}

export default Navigation