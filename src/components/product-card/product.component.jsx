import './product.styles.scss';

import Button from '../button/button.component';
import { useCartContext } from '../../contexts/cart.context';

const ProductCard = ({
  product

}) => {
  const { addItems } = useCartContext();
  const {
    name,
    price,
    imageUrl
  } = product;

  return (
    <div className='product-card-container' >
      <img
        src={imageUrl}
        alt={`${name}`}
      />
      <div className='footer' >
        <span className='name' > {name}</span>
        <span className='price' > {price}</span>
      </div>
      <Button buttonType='inverted' onClick={() => addItems(product)}>Add to card</Button>
    </div>);
}

  ;

export default ProductCard;