import React from 'react'
import { ReactComponent as ShoppingIcon } from '../../assets/shopping-bag.svg';
import { useCartContext } from '../../contexts/cart.context';
import './cart-icon.styles.scss'
function CartIcon() {
  const { isCartOpen, setCartOpen, countCartItems } = useCartContext();
  return (
    <div className='cart-icon-container' onClick={()=>setCartOpen(!isCartOpen)}>
      <ShoppingIcon className='shopping-icon' />
      <span>{countCartItems}</span>
    </div>
  )
}

export default CartIcon