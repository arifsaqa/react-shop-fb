import React from 'react'
import { useCartContext } from '../../contexts/cart.context';
import Button from '../button/button.component'
import CartItem from '../cart-item/cart-item.component';
import './cart-dropdown.styles.scss';
function CartDropdown() {
  const { cartItems,  } = useCartContext();
  return (
    <div className='cart-dropdown-container'>
      <div className='cart-items'>
        {cartItems.map((item) => <CartItem key={item.id} cartItem={item} />)}
      </div>
      <Button>Checkout</Button>
    </div>
  )
}

export default CartDropdown